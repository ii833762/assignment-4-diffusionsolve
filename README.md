# Introduction to Numerical Modelling
# Assignment 4 - Solving the diffusion equation
# Calculates and plots the diffusion equation

import numpy as np
import matplotlib.pyplot as plt

from DiffusionSetup import *

# Define diffusion equation code inside a function to avoid global variables
def diffsolve():
    """Solve the diffusion equation"""
    
    # Define the parameters
    zmin = 0                    # height of bottom boundary (m)
    zmax = 1e3                  # height of top boundary (m)
    nz = 21                     # number of vertical points
    dz = (zmax-zmin)/(nz-1)     # size of vertical step (m)
    nt = 20                     # number of time steps
    dt = 600                    # size of time step (s)
    K = 1                       # thermal diffusivity coefficient (m^2/s)
    d = K*dt/dz**2              # dimensionless diffusion coefficient
    T0 = 293                    # initial uniform temperature (K)
    Q = -1.5/86400              # rate of atmospheric cooling (K/s)
    sumt = nt*dt                # total time (s)
    
    # Create the z domain
    z = np.linspace(zmin,zmax,nz)
    
    # Initial uniform temperature
    T = T0*np.ones(nz)
    
    print ("Non-dimensional diffusion coefficient =", d)
    print ("Number of vertical points =", nz)
    print ("Size of vertical step =", dz)
    print ("Number of time steps =", nt)
    print ("Size of time steps =", dt)
    print ("End time =", sumt)
    
    # Diffusion using FTCS and BTCS
    FTCSofT = FTCS_fixed_zerograd(T.copy(), K, Q, dz, nt, dt)
    BTCSofT = BTCS_fixed_zerograd(T.copy(), K, Q, dz, nt, dt)
    
    # Plot the solutions
    plt.plot(FTCSofT-T0, z, label="FTCS")
    plt.plot(BTCSofT-T0, z, label="BTCS")
    plt.legend()
    plt.title("Change in vertical temperature profile after 12000 seconds")
    plt.xlabel("Change in temperature (K)")
    plt.ylabel("Height (m)")
    plt.xlim(-0.225,0)
    plt.ylim(0,1000)
    plt.tight_layout()
    plt.show()
    
diffsolve()
